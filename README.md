

# A3D : Ray Tracing

> LANGLAIS Constance - M1 INFO



## Archive detail

This archive contains the files made for the A3D project on Ray tracing.
It includes the present `README.md`, the `.java` files in the `src` directory and some images of the project in `images`.



## Compilation

A `makefile` is also provided in order to facilitate compilation and execution :

- `make` : execute the compilation command and put the result in a `classes` directory

- `make runTga` : run the program with no arguments.
  It will simply generate a TGA file.
  
- `make runTgaDisplay` : run the program with `--display` argument, launching a display window to visualize directly the result.

  

## Classes

Regarding the Java classes, they are all documented. We invite you to consult the documentation provided for more details.
However, here are some indications to facilitate the understanding:

- The `JavaTga` class contains the main function that builds a scene via predefined methods, and then renders it.
  It is then in this method that the ray tracing loop is performed.
- The `Scene` class gathers all the elements of a scene such as spheres, planes, triangles and lights.
  It is the `render` method of `Scene` which takes care of the ray tracing loop studied in class.
  It then delegates the processing to other methods or to the classes involved.

The other classes seem self-explanatory enough not to be detailed further.



## Realizations

All the **requested features** have been managed.
A single scene was realized by arranging the objects on **both sides of the observer**.
It is possible to **change the look** of the observer by **modifying the boolean** `lookFront` in the main function.
Many **combinations** of colors, transparency and reflection were tested.

The number of ray levels is given in the `Scene` class and can be changed there.
All the images configurations are done with `10` rays in addition to primary rays.

The distance to screen is set in `JavaTga` to `2.0` (for all the images configurations).



## Images configuration

> **Notes** : the front scenes configuration images have been done before the back elements have been put, this is why there are small differences.
>
> The effect of cutting the light due to the chandelier is desired.
> Indeed, the light is located at the level of the chandelier for the front of the room, which gives more realism in my opinion.



- `Scene_front.png` : The given scene, no need to change things. - Observer at `(0.5f, 1f, 0f)`
- `Scene_front_mirror.png` : To better see the mirror sphere - Observer at `(1f, 0.8f, -1.5f)`
- `Scene_front_diamond.png` : To better see the diamond sphere - Observer at `(0.4f, 0.6f, -0.9)`
- `Scene_front_transp_mirror.png` : To better see the transparent/mirror sphere - Observer at `(-0.5f, 0.6f, -1.3f)`
- `Scene_back.png` : The given scene from back view. - Observer at `(0.5f, 1f, 0f)` (`lookFront = false`)
- `Scene_back_chroma_circle.png` : To better see the chromatic circle. - Observer at `(0.5f, 1f, 2f)` (`lookFront = false`)

> Special thanks to my sister for wanting to test the addition of colors by making the very nice chromatic circle that decorates the back of the room !