# LANGLAIS Constance M1 INFO

JC = javac
JVM = java
.SUFFIXES: .java .class
.java.class:
	$(JC) $(JFLAGS) $*.java
	
SRC = src
OBJECTS = $(SRC)/objects
UTILS = $(SRC)/utils
CLASSEDIR = classes
MAIN_TGA = JavaTga

JAVA_FILES = $(SRC)/*.java $(OBJECTS)/*.java $(UTILS)/*.java
JFLAGS = -Xlint:all -Xdiags:verbose

.PHONY: clean

all : sceneTgaJava 

sceneTgaJava:
	$(JC) $(JFLAGS) -d $(CLASSEDIR) $(JAVA_FILES)

runTga :
	$(JVM) -classpath $(CLASSEDIR) $(MAIN_TGA)

runTgaDisplay:
	$(JVM) -classpath $(CLASSEDIR) $(MAIN_TGA) --display

clean:
	$(RM) -r $(CLASSEDIR)
	
