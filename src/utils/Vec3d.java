package utils;

import java.util.Objects;

/**
 * Basic class to represent 3-vectors Not intended to be a complete algebra class ! Methods are studied to
 * avoid allocation of new objects, this explains some weird choices...
 */
public class Vec3d {
    /// The x coordinate
    private double x;
    /// The y coordinate
    private double y;
    /// The z coordinate
    private double z;

    /**
     * Create a null vector.
     */
    public Vec3d() {
        this(0, 0, 0);
    }

    /**
     * Construct a vector from one point to another.
     *
     * @param point1 The origin point.
     * @param point2 The destination point.
     */
    public Vec3d(final Vec3d point1, final Vec3d point2) {
        this.x = point2.x - point1.x;
        this.y = point2.y - point1.y;
        this.z = point2.z - point1.z;
    }

    /**
     * Construct a vector by giving its coordinates.
     *
     * @param x The x coordinate.
     * @param y The y coordinate.
     * @param z The z coordinate.
     */
    public Vec3d(final double x, final double y, final double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Copy a vector in the current one.
     *
     * @param vector The vector to copy.
     */
    public Vec3d(final Vec3d vector) {
        this.x = vector.x;
        this.y = vector.y;
        this.z = vector.z;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Vec3d vec3f = (Vec3d) o;
        return Double.compare(vec3f.x, x) == 0 && Double.compare(vec3f.y, y) == 0 && Double.compare(vec3f.z,
                z) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, z);
    }

    public Vec3d set(final Vec3d vector) {
        this.x = vector.x;
        this.y = vector.y;
        this.z = vector.z;
        return this;
    }

    /**
     * Set the current vector with the given coordinates.
     *
     * @param x The x coordinate.
     * @param y The y coordinate.
     * @param z The z coordinate.
     * @return The current vector with the new coordinates.
     */
    public Vec3d set(final double x, final double y, final double z) {
        this.x = x;
        this.y = y;
        this.z = z;
        return this;
    }

    /**
     * Compute the square of the length of the current vector.
     *
     * @return The length square of the current vector.
     */
    public double lengthSquare() {
        return this.x * this.x + this.y * this.y + this.z * this.z;
    }

    /**
     * Compute the length of the current vector.
     *
     * @return The length square of the current vector.
     */
    public double length() {
        return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
    }

    /**
     * Normalize the current vector.
     *
     * @return The current vector normalized.
     */
    public Vec3d normalize() {
        double l = this.lengthSquare();
        if (l == 0) {
            return this;
        }
        l = Math.sqrt(l);
        return this.scale(1 / l);
    }

    /**
     * Add a vector to the current one.
     *
     * @param v The vector to add to the current one.
     * @return The current vector added to the vector {@code v}.
     */
    public Vec3d add(final Vec3d v) {
        this.x += v.x;
        this.y += v.y;
        this.z += v.z;
        return this;
    }

    /**
     * Add two vectors.
     *
     * @param v1 The first vector.
     * @param v2 The second vector.
     * @return The addition of the two vectors.
     */
    public static Vec3d add(final Vec3d v1, final Vec3d v2) {
        return new Vec3d(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
    }

    /**
     * Subtract a vector to the current one.
     *
     * @param v The vector to subtract to the current one.
     * @return The current vector subtracted by the vector {@code v}.
     */
    public Vec3d sub(final Vec3d v) {
        this.x -= v.x;
        this.y -= v.y;
        this.z -= v.z;
        return this;
    }

    /**
     * Subtract two vectors.
     *
     * @param v1 The first vector.
     * @param v2 The second vector.
     * @return The subtraction of the two vectors.
     */
    public static Vec3d sub(final Vec3d v1, final Vec3d v2) {
        return new Vec3d(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
    }

    /**
     * Scale the current vector.
     *
     * @param scale The scale to apply.
     * @return The current vector scaled.
     */
    public Vec3d scale(final double scale) {
        this.x *= scale;
        this.y *= scale;
        this.z *= scale;
        return this;
    }

    /**
     * Scale a vector.
     *
     * @param v     The vector to scale.
     * @param scale The scale to apply.
     * @return The scaled vector.
     */
    public static Vec3d scale(final Vec3d v, final double scale) {
        return new Vec3d(v.x * scale, v.y * scale, v.z * scale);
    }

    /**
     * Compute the dot product of the current vector with another one.
     *
     * @param v The vector to compute the dot product with.
     * @return The dot product of the two vectors.
     */
    public double dotProduct(final Vec3d v) {
        return this.x * v.x + this.y * v.y + this.z * v.z;
    }

    /**
     * Compute the dot product of two vectors.
     *
     * @param v1 The first vector of the product.
     * @param v2 The second vector of the product.
     * @return The dot product of the two vectors.
     */
    public static double dotProduct(final Vec3d v1, final Vec3d v2) {
        return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
    }

    /**
     * Compute the cross product of the current vector with another one.
     *
     * @param v The vector to compute the cross product with.
     * @return The cross product of the two vectors.
     */
    public Vec3d crossProduct(final Vec3d v) {
        return new Vec3d(this.y * v.z - this.z * v.y,
                this.z * v.x - this.x * v.z,
                this.x * v.y - this.y * v.x);
    }

    /**
     * Compute the cross product of two vectors.
     *
     * @param v1 The first vector of the product.
     * @param v2 The second vector of the product.
     * @return The cross product of the two vectors.
     */
    public static Vec3d crossProduct(final Vec3d v1, final Vec3d v2) {
        return new Vec3d(v1.y * v2.z - v1.z * v2.y,
                v1.z * v2.x - v1.x * v2.z,
                v1.x * v2.y - v1.y * v2.x);
    }

    /**
     * @param index The index of the coordinate of the vector to get.
     * @return The coordinate corresponding to the given index.
     */
    public double get(final int index) {
        if (index == 0) {
            return x;
        }
        if (index == 1) {
            return y;
        }
        if (index == 2) {
            return z;
        }
        throw new IllegalArgumentException("Index should be between 0 and 2.");
    }

    /**
     * @param index The index of the coordinate of the vector to set.
     * @param value The new value of the coordinate corresponding to the given index.
     */
    public void set(final int index, final double value) {
        if (index == 0) {
            this.x = value;
        } else if (index == 1) {
            this.y = value;
        } else if (index == 2) {
            this.z = value;
        } else {
            throw new IllegalArgumentException("Index should be between 0 and 2.");
        }
    }
}
