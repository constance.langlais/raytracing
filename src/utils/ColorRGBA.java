package utils;

import java.awt.*;

/**
 * Utility class representing a color class, inspired from {@link Color} class. Provide methods for simple
 * color calculation based on RGBA components between 0 and 1. Components are clamped automatically after each
 * calculation.
 */
public class ColorRGBA {
    public static final ColorRGBA WHITE = new ColorRGBA(255, 255, 255);
    public static final ColorRGBA LIGHT_GRAY = new ColorRGBA(192, 192, 192);
    public static final ColorRGBA GRAY = new ColorRGBA(128, 128, 128);
    public static final ColorRGBA DARK_GRAY = new ColorRGBA(64, 64, 64);
    public static final ColorRGBA BLACK = new ColorRGBA(0, 0, 0);
    public static final ColorRGBA RED = new ColorRGBA(255, 0, 0);
    public static final ColorRGBA PINK = new ColorRGBA(255, 175, 175);
    public static final ColorRGBA ORANGE = new ColorRGBA(255, 200, 0);
    public static final ColorRGBA YELLOW = new ColorRGBA(255, 255, 0);
    public static final ColorRGBA GREEN = new ColorRGBA(0, 255, 0);
    public static final ColorRGBA MAGENTA = new ColorRGBA(255, 0, 255);
    public static final ColorRGBA CYAN = new ColorRGBA(0, 255, 255);
    public static final ColorRGBA BLUE = new ColorRGBA(0, 0, 255);

    /// The red component
    private float red;
    /// The green component
    private float green;
    /// The blue component
    private float blue;
    /// The alpha component
    private float alpha;

    /**
     * Create a color from integer RGB components between 0 and 255. Alpha component is set to 255.
     *
     * @param r The red component.
     * @param g The green component.
     * @param b The blue component.
     */
    public ColorRGBA(int r, int g, int b) {
        this(r, g, b, 255);
    }

    /**
     * Create a color from integer RGBA components between 0 and 255.
     *
     * @param r The red component.
     * @param g The green component.
     * @param b The blue component.
     * @param a The alpha component.
     */
    public ColorRGBA(int r, int g, int b, int a) {
        this(r / 255f, g / 255f, b / 255f, a / 255f);
    }

    /**
     * Create a color from float RGB components between 0 and 1. Alpha is set to 1.
     *
     * @param r The red component.
     * @param g The green component.
     * @param b The blue component.
     */
    public ColorRGBA(float r, float g, float b) {
        this(r, g, b, 1);
    }

    /**
     * Create a color from float RGBA components between 0 and 1.
     *
     * @param r The red component.
     * @param g The green component.
     * @param b The blue component.
     * @param a The alpha component.
     */
    public ColorRGBA(float r, float g, float b, float a) {
        this.red = r;
        this.green = g;
        this.blue = b;
        this.alpha = a;
        clamp();
    }

    /**
     * Create a color from a {@link Color}.
     *
     * @param color The color to create the utils.ColorRGBA.
     */
    public ColorRGBA(Color color) {
        float[] rgbComponents = color.getRGBComponents(null);
        this.red = rgbComponents[0];
        this.green = rgbComponents[1];
        this.blue = rgbComponents[2];
        this.alpha = rgbComponents[3];
    }

    /**
     * Add a color to the current color and return the current color after calculation.
     *
     * @param color The color to add to the current color.
     * @return The current color after calculation.
     */
    public ColorRGBA add(ColorRGBA color) {
        this.red += color.red;
        this.green += color.green;
        this.blue += color.blue;
        this.alpha += color.alpha;
        clamp();
        return this;
    }

    /**
     * Add a two colors return a new color after calculation.
     *
     * @param color1 The first color.
     * @param color2 The second color.
     * @return The result of the addition of the two colors.
     */
    public static ColorRGBA add(ColorRGBA color1, ColorRGBA color2) {
        return new ColorRGBA(
                color1.red + color2.red,
                color1.green + color2.green,
                color1.blue + color2.blue,
                color1.alpha + color2.alpha);
    }

    /**
     * Multiply a color by a scalar and return the current color after calculation.
     *
     * @param scalar The scalar to multiply the current color by.
     * @return The current color after calculation.
     */
    public ColorRGBA mult(float scalar) {
        this.red *= scalar;
        this.green *= scalar;
        this.blue *= scalar;
        this.alpha *= scalar;
        clamp();
        return this;
    }

    /**
     * Multiply a color by a scalar and return a new color after calculation.
     *
     * @param color  The color to multiply.
     * @param scalar The scalar.
     * @return The result of the multiplication of the two colors.
     */
    public static ColorRGBA mult(ColorRGBA color, float scalar) {
        return new ColorRGBA(
                color.red * scalar,
                color.green * scalar,
                color.blue * scalar,
                color.alpha * scalar);
    }

    /**
     * Multiply a color by another color and return the current color after calculation.
     *
     * @param color The color to multiply the current color by.
     * @return The current color after calculation.
     */
    public ColorRGBA mult(ColorRGBA color) {
        this.red *= color.red;
        this.green *= color.green;
        this.blue *= color.blue;
        this.alpha *= color.alpha;
        clamp();
        return this;
    }

    /**
     * Multiply a two colors return a new color after calculation.
     *
     * @param color1 The first color.
     * @param color2 The second color.
     * @return The result of the multiplication of the two colors.
     */
    public static ColorRGBA mult(ColorRGBA color1, ColorRGBA color2) {
        return new ColorRGBA(
                color1.red * color2.red,
                color1.green * color2.green,
                color1.blue * color2.blue,
                color1.alpha * color2.alpha);
    }

    /**
     * @return The {@link Color} corresponding to the current color.
     */
    public Color getColor() {
        return new Color(red, green, blue, alpha);
    }

    /**
     * Clamp the current color between 0 and 1.
     */
    private void clamp() {
        red = MathUtils.clamp(red, 0, 1);
        green = MathUtils.clamp(green, 0, 1);
        blue = MathUtils.clamp(blue, 0, 1);
        alpha = MathUtils.clamp(alpha, 0, 1);
    }

    /**
     * @return The red component.
     */
    public float getRed() {
        return red;
    }

    /**
     * @param red The red component.
     */
    public void setRed(final float red) {
        this.red = red;
    }

    /**
     * @return The green component.
     */
    public float getGreen() {
        return green;
    }

    /**
     * @param green The green component.
     */
    public void setGreen(final float green) {
        this.green = green;
    }

    /**
     * @return The blue component.
     */
    public float getBlue() {
        return blue;
    }

    /**
     * @param blue The blue component.
     */
    public void setBlue(final float blue) {
        this.blue = blue;
    }

    /**
     * @return The alpha component.
     */
    public float getAlpha() {
        return alpha;
    }

    /**
     * @param alpha The alpha component.
     */
    public void setAlpha(final float alpha) {
        this.alpha = alpha;
    }
}
