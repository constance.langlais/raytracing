package utils;

/**
 * Utility class providing mathematical methods.
 */
public class MathUtils {
    /// The range of error to take in account when comparing to 0
    public static final float ERROR_RANGE = 0.0001f;

    /**
     * Private constructor to hide the implicit public one.
     */
    private MathUtils() {
    }

    /**
     * Clamp a value between a minimum and a maximum value.
     *
     * @param val The value to clamp.
     * @param min The minimum value.
     * @param max The maximum value.
     * @return The value clamped.
     */
    public static float clamp(float val, int min, int max) {
        return Math.max(min, Math.min(max, val));
    }
}
