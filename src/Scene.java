import objects.AbstractModelObject;
import objects.RefractionMiddle;
import utils.ColorRGBA;
import utils.MathUtils;
import utils.Vec3d;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

/**
 * Class representing a scene containing one or multiple objects and lights, and the position of the observer.
 * Contains the main ray tracing method by finding the color of each pixel in order to fill an image buffer.
 */
public class Scene {
    /**
     * Specific class representing the nearest object, with the minimum lambda corresponding to a ray cast
     * intersection with an object of the scene.
     */
    private static class MinimumObject {
        /// The lambda value of the object
        public final double lambdaMin;
        /// The object corresponding to the nearest one
        public final AbstractModelObject objectMin;

        public MinimumObject(final double lambdaMin, final AbstractModelObject objectMin) {
            this.lambdaMin = lambdaMin;
            this.objectMin = objectMin;
        }
    }

    /**
     * Specific class representing a refracted ray with its direction and the middle it is cast from.
     */
    private static class RefractedRay {
        /// The direction of the ray
        public final Vec3d direction;
        /// The physical middle the ray is cast from
        public final RefractionMiddle middle;

        private RefractedRay(final Vec3d direction, final RefractionMiddle middle) {
            this.direction = direction;
            this.middle = middle;
        }
    }

    /// The position of the observer in the scene
    private final Vec3d observerPosition;
    /// The set of objects present in the scene
    private final List<AbstractModelObject> objects;
    /// The set of lights present in the scene
    private final List<Light> lights;
    /// The global scene physical middle, useful for refraction
    private RefractionMiddle globalMiddle;
    /// The maximum number of rays that can be recursively cast
    private static final int MAX_RAY_LEVEL = 10;

    /**
     * Construct an empty scene.
     */
    public Scene() {
        this.observerPosition = new Vec3d(0, 0, 0);
        this.objects = new ArrayList<>();
        this.lights = new ArrayList<>();
        this.globalMiddle = RefractionMiddle.AIR;
    }

    /**
     * Add an object to the current scene.
     *
     * @param object The object to add to the scene.
     */
    public void addObject(AbstractModelObject object) {
        this.objects.add(object);
    }

    /**
     * Add a light to the current scene.
     *
     * @param light The light to add to the scene.
     */
    public void addLight(Light light) {
        this.lights.add(light);
    }


    /**
     * Render the scene by filling buffers representing pixels color of an image. The color is defined by ray
     * tracing method, which is recursive.
     *
     * @param width            The width of the final image.
     * @param height           The height of the final image.
     * @param distanceToScreen The distance where the projection plane is placed.
     * @return The buffers corresponding to the rendered image. - {@link BufferedImage} is useful for
     * displaying the image in a panel. - {@code byte[]} buffer is useful for exporting to TGA format.
     */
    public BuffersData render(final int width, final int height, final double distanceToScreen) {
        byte[] buffer = new byte[3 * width * height];
        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);

        for (int row = 0; row < height; row++) { // for each row of the image
            for (int col = 0; col < width; col++) { // for each column of the image
                Vec3d rayDirection = new Vec3d(
                        1f / width * (col - width / 2f),
                        1f / width * (row - height / 2f),
                        -distanceToScreen);
                ColorRGBA color = findColor(observerPosition, rayDirection, globalMiddle, 0);
                Color pixelColor = color.getColor();

                // Fill buffers
                bufferedImage.setRGB(col, height - row - 1, pixelColor.getRGB());
                int index = 3 * ((row * width) + col);
                buffer[index] = (byte) pixelColor.getBlue();
                buffer[index + 1] = (byte) pixelColor.getGreen();
                buffer[index + 2] = (byte) pixelColor.getRed();
            }
        }
        return new BuffersData(buffer, bufferedImage);
    }

    /**
     * Find the final color of the pixel. Cast primary rays to find the first intersected objects, and the
     * shadow rays to compute lights and shadows. Then reflected and refracted rays are cast depending on the
     * intersected object properties.
     *
     * @param originPoint       The point the ray is cast from.
     * @param rayDirection      The direction the ray is cast.
     * @param incomingRayMiddle The physical middle the ray is cast from.
     * @param level             The level of ray cast corresponding to how many rays have been cast before.
     * @return The final color of the pixel.
     */
    private ColorRGBA findColor(Vec3d originPoint, Vec3d rayDirection,
                                RefractionMiddle incomingRayMiddle, Integer level) {
        // 1. Cast primary rays
        MinimumObject min = tracePrimaryRay(originPoint, rayDirection);
        double lambdaMin = min.lambdaMin;
        AbstractModelObject objectMin = min.objectMin;

        // No objects in the ray direction
        if (objectMin == null) {
            return ColorRGBA.BLACK;
        }

        Vec3d intersection = Vec3d.add(originPoint, Vec3d.scale(rayDirection, lambdaMin));
        Vec3d normal = objectMin.getNormalAt(intersection).normalize();
        ColorRGBA color = Light.computeAmbientColor(objectMin.getMaterialColor());

        // 2. Compute light components colors and shadows
        addLightComponents(objectMin, intersection, normal, color);

        // 3. Cast reflected and refracted ray depending on the object properties
        if (level < MAX_RAY_LEVEL) {
            // The object is reflective
            if (objectMin.isReflective()) {
                Vec3d r = computeReflectedRay(rayDirection, normal);
                color.add(utils.ColorRGBA.mult(findColor(intersection, r, incomingRayMiddle, level + 1),
                        objectMin.getKr()));
            }

            if (objectMin.isTransparent()) {
                // The object is transparent
                RefractedRay outRay = computeRefractedRay(rayDirection, normal,
                        incomingRayMiddle, objectMin.getMiddle());
                color.add(utils.ColorRGBA.mult(findColor(intersection, outRay.direction, outRay.middle,
                                level + 1), objectMin.getKt()));
            }
        }
        return color;
    }

    /**
     * Trace primary rays allowing to determine the nearest object encountered by a ray.
     *
     * @param originPoint  The point the ray is cast from.
     * @param rayDirection The direction of the ray.
     * @return The nearest object the intersected the ray cast.
     */
    private MinimumObject tracePrimaryRay(final Vec3d originPoint, final Vec3d rayDirection) {
        double lambdaMin = Double.MAX_VALUE;
        AbstractModelObject objectMin = null;
        // Find the nearest intersection between the ray and a scene object
        for (final AbstractModelObject object : objects) {
            Double lambda = object.computeIntersection(originPoint, rayDirection);
            if (lambda != null && lambda > MathUtils.ERROR_RANGE && lambda < lambdaMin) {
                lambdaMin = lambda;
                objectMin = object;
            }
        }
        return new MinimumObject(lambdaMin, objectMin);
    }

    /**
     * Add the lights components to to the current pixel color by using object visibility for shadow and Phong
     * method for lighting.
     *
     * @param objectMin    The object that is intersected.
     * @param intersection The intersection point.
     * @param normal       The normal to the intersection point.
     * @param color        The current color of the pixel.
     */
    private void addLightComponents(final AbstractModelObject objectMin, final Vec3d intersection,
                                    final Vec3d normal, final ColorRGBA color) {
        for (final Light light : lights) {
            boolean visible = true;
            // Compute object visibility
            Vec3d interToSource = new Vec3d(intersection, light.getPosition());
            for (final AbstractModelObject object : objects) {
                Double lambda = object.computeIntersection(intersection, interToSource);
                if (lambda != null && MathUtils.ERROR_RANGE < lambda && lambda < 1 - MathUtils.ERROR_RANGE) {
                    visible = false;
                    break;
                }
            }

            // Compute light effects when visible, else is makes shadows
            if (visible) {
                ColorRGBA diffuse = light.computeDiffuseColor(intersection, normal,
                        objectMin.getMaterialColor());
                ColorRGBA specular = light.computeSpecularColor(observerPosition, intersection, normal,
                        objectMin.getSpecularColor(), objectMin.getShininess());
                float attenuation = light.computeAttenuation(intersection);
                color.add(ColorRGBA.add(diffuse, specular).mult(attenuation));
            }
        }
    }

    /**
     * Compute a reflected ray from an intersection point of an object.
     *
     * @param rayDirection The direction of the incoming ray.
     * @param normal       The normal to the intersection point of the object.
     * @return The direction of the reflected ray.
     */
    private static Vec3d computeReflectedRay(final Vec3d rayDirection, final Vec3d normal) {
        return Vec3d.sub(rayDirection, Vec3d.scale(normal, 2 * Vec3d.dotProduct(rayDirection, normal)));
    }

    /**
     * Compute a refracted ray from an intersection point of an object.
     *
     * @param rayDirection      The direction of the incoming ray.
     * @param normal            The normal to the intersection point at the object.
     * @param incomingRayMiddle The physical middle the incoming ray is from.
     * @param objectMiddle      The middle of the intersected object.
     * @return The direction of the refracted ray and the middle it will be cast from.
     */
    private static RefractedRay computeRefractedRay(final Vec3d rayDirection, final Vec3d normal,
                                                    RefractionMiddle incomingRayMiddle,
                                                    RefractionMiddle objectMiddle) {
        RefractionMiddle outMiddle;
        float cosi = MathUtils.clamp((float) Vec3d.dotProduct(rayDirection, normal), -1, 1);
        float eta;
        if (cosi < 0) {
            // The ray was cast outside the surface
            outMiddle = objectMiddle;
            cosi = -cosi;
            eta = incomingRayMiddle.ior / objectMiddle.ior;
        } else {
            // The ray was cast inside the surface
            outMiddle = incomingRayMiddle;
            eta = objectMiddle.ior / incomingRayMiddle.ior;
            normal.scale(-1);
        }

        float k = 1 - eta * eta * (1 - cosi * cosi);
        Vec3d t = k < 0 ? new Vec3d(0, 0, 0)
                : Vec3d.add(Vec3d.scale(rayDirection, eta),
                Vec3d.scale(normal, eta * cosi - Math.sqrt(k)));
        return new RefractedRay(t, outMiddle);
    }

    /**
     * @param x The x coordinate of the position of the observer.
     * @param y The y coordinate of the position of the observer.
     * @param z The z coordinate of the position of the observer.
     */
    public void setObserver(final float x, final float y, final float z) {
        this.observerPosition.set(x, y, z);
    }

    /**
     * @param globalMiddle The global scene middle.
     */
    public void setGlobalMiddle(RefractionMiddle globalMiddle) {
        this.globalMiddle = globalMiddle;
    }
}
