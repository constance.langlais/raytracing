import java.awt.image.BufferedImage;

/**
 * Class representing the same image data in different types of buffers.
 */
public class BuffersData {
    /// The bytes buffer to allow TGA conversion
    private final byte[] bytesBuffer;
    /// The buffer used to display the image
    private final BufferedImage bufferedImage;

    /**
     * Construct a BufferData with the buffers corresponding to the same image data.
     *
     * @param bytesBuffer   The bytes buffer.
     * @param bufferedImage The {@link BufferedImage}.
     */
    public BuffersData(final byte[] bytesBuffer, final BufferedImage bufferedImage) {
        this.bytesBuffer = bytesBuffer;
        this.bufferedImage = bufferedImage;
    }

    /**
     * @return The byte buffer to allow TGA conversion.
     */
    public byte[] getBytesBuffer() {
        return bytesBuffer;
    }

    /**
     * @return The buffer used to display the image.
     */
    public BufferedImage getBufferedImage() {
        return bufferedImage;
    }
}
