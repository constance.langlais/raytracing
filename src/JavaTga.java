import objects.Plane;
import objects.RefractionMiddle;
import objects.Sphere;
import objects.Triangle;
import utils.ColorRGBA;
import utils.Vec3d;

import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Objects;

/**
 * @author P. Meseure based on a Java Adaptation of a C code by B. Debouchages (M1, 2018-2019)
 */
public class JavaTga {
    /// The width of the image
    private static final int WIDTH = 1024;
    /// The height of the image
    private static final int HEIGHT = 768;
    /// The distance to the screen
    private static final double DISTANCE_TO_SCREEN = 0.2f;
    /// The scene to create the image from
    private static final Scene scene = new Scene();

    /**
     * @param fout : output file stream
     * @param n    : short to write to disc in little endian
     */
    private static void writeShort(FileOutputStream fout, int n) throws IOException {
        fout.write(n & 255);
        fout.write((n >> 8) & 255);
    }

    /**
     * @param filename name of final TGA file
     * @param buffer   buffer that contains the image. 3 bytes per pixel ordered this way : Blue, Green, Red
     * @param width    Width of the image
     * @param height   Height of the image
     */
    private static void saveTGA(String filename, byte buffer[], int width, int height) throws IOException {

        FileOutputStream fout = new FileOutputStream(filename);

        fout.write(0); // Comment size, no comment
        fout.write(0); // Colormap type: No colormap
        fout.write(2); // Image type
        writeShort(fout, 0); // Origin
        writeShort(fout, 0); // Length
        fout.write(0); // Depth
        writeShort(fout, 0); // X origin
        writeShort(fout, 0); // Y origin
        writeShort(fout, width); // Width of the image
        writeShort(fout, height); // Height of the image
        fout.write(24); // Pixel size in bits (24bpp)
        fout.write(0); // Descriptor

        /* Write the buffer */
        fout.write(buffer);
        fout.close();
    }


    /**
     * @param args no command line arguments
     */
    public static void main(String[] args) {
        boolean lookFront = true;

        // Initialize scene objects
        scene.setObserver(0f, 1f, 0f);
        createCenteredRoom(4f, 6f, 2.3f);
        createSpheres();
        createChandelier();
        createMirrorTriangles();
        createLights();

        // Render the scene by using ray tracing method
        double D = lookFront ? DISTANCE_TO_SCREEN : -DISTANCE_TO_SCREEN;
        BuffersData buffers = scene.render(WIDTH, HEIGHT, D);

        try {
            saveTGA("LANGLAIS_scene.tga", buffers.getBytesBuffer(), WIDTH, HEIGHT);
        } catch (Exception e) {
            System.err.println("TGA file not created :" + e);
        }

        if (args.length >= 1) {
            if (args.length == 1 && Objects.equals(args[0], "--display")) {
                displayImage(buffers.getBufferedImage());
            } else {
                System.err.println("Wrong arguments provided : use --display to display the image in a " +
                        "frame");
            }
        }
    }

    /**
     * Display an image from a {@link BufferedImage} in a frame.
     *
     * @param bufferedImage The buffer of the image to display.
     */
    private static void displayImage(final BufferedImage bufferedImage) {
        JFrame frame = new JFrame("Algorithmique 3D - Scene");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.getContentPane().add(new JLabel(new ImageIcon(bufferedImage)));
        frame.setSize(WIDTH, HEIGHT);
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Create a room in the scene by giving its dimensions.
     *
     * @param width  The width of the room.
     * @param depth  The depth of the room.
     * @param height The height of the room.
     */
    private static void createCenteredRoom(final float width, final float depth, final float height) {
        float d = depth / 2;
        float w = width / 2;

        Plane front = new Plane(new Vec3d(0, 0, -d), new Vec3d(0, 0, 1));
        front.setMaterialColor(ColorRGBA.LIGHT_GRAY);
        front.setShininess(25);

        Plane back = new Plane(new Vec3d(0, 0, d), new Vec3d(0, 0, -1));
        back.setMaterialColor(ColorRGBA.WHITE);
        back.setShininess(25);

        Plane left = new Plane(new Vec3d(-w, 0, 0), new Vec3d(1, 0, 0));
        left.setMaterialColor(ColorRGBA.RED);
        left.setShininess(25);

        Plane right = new Plane(new Vec3d(w, 0, 0), new Vec3d(-1, 0, 0));
        right.setMaterialColor(ColorRGBA.GREEN);
        right.setShininess(25);

        Plane ceiling = new Plane(new Vec3d(0, height, 0), new Vec3d(0, -1, 0));
        ceiling.setMaterialColor(ColorRGBA.CYAN);
        ceiling.setShininess(500);

        Plane floor = new Plane(new Vec3d(0, 0, 0), new Vec3d(0, 1, 0));
        floor.setMaterialColor(ColorRGBA.DARK_GRAY);
        floor.setReflectiveCoef(0.5f);
        floor.setTransparencyCoef(0.5f);
        floor.setMiddle(RefractionMiddle.WATER);

        scene.addObject(front);
        scene.addObject(back);
        scene.addObject(left);
        scene.addObject(right);
        scene.addObject(ceiling);
        scene.addObject(floor);
    }

    /**
     * Create the spheres of the room and place them.
     */
    private static void createSpheres() {
        // Front
        Sphere diamondSphere = new Sphere(new Vec3d(0.4, 0.4, -1.5), 0.4);
        diamondSphere.setMaterialColor(ColorRGBA.WHITE);
        diamondSphere.setTransparencyCoef(0.7f);
        diamondSphere.setMiddle(RefractionMiddle.DIAMOND);

        Sphere halfMirrorTransSphere = new Sphere(new Vec3d(-0.5, 0.5, -2), 0.5);
        halfMirrorTransSphere.setMaterialColor(ColorRGBA.MAGENTA);
        halfMirrorTransSphere.setReflectiveCoef(0.5f);
        halfMirrorTransSphere.setTransparencyCoef(0.5f);
        halfMirrorTransSphere.setShininess(500);

        Sphere mirrorSphere = new Sphere(new Vec3d(1, 0.7, -2.5), 0.7);
        mirrorSphere.setReflectiveCoef(1f);
        mirrorSphere.setShininess(100);

        Sphere transparentSphere = new Sphere(new Vec3d(-0.8, 0.3, -1.3), 0.3);
        transparentSphere.setMaterialColor(ColorRGBA.YELLOW);
        transparentSphere.setTransparencyCoef(0.8f);

        Sphere solidSphere = new Sphere(new Vec3d(0, 0.2, -1.1), 0.2);
        solidSphere.setMaterialColor(ColorRGBA.PINK);

        scene.addObject(diamondSphere);
        scene.addObject(halfMirrorTransSphere);
        scene.addObject(mirrorSphere);
        scene.addObject(transparentSphere);
        scene.addObject(solidSphere);

        // Back
        mirrorSphere = new Sphere(new Vec3d(0.3, 0.4, 1.5), 0.3);
        mirrorSphere.setMaterialColor(ColorRGBA.RED);
        mirrorSphere.setTransparencyCoef(0.8f);
        mirrorSphere.setMiddle(RefractionMiddle.WATER);
        mirrorSphere.setShininess(100);
        scene.addObject(mirrorSphere);

        mirrorSphere = new Sphere(new Vec3d(-0.7, 0.7, 2), 0.5);
        mirrorSphere.setReflectiveCoef(0.5f);
        scene.addObject(mirrorSphere);

        mirrorSphere = new Sphere(new Vec3d(1.2, 0.8, 2.5), 0.6);
        mirrorSphere.setReflectiveCoef(0.5f);
        mirrorSphere.setMaterialColor(ColorRGBA.CYAN);
        mirrorSphere.setTransparencyCoef(0.8f);
        mirrorSphere.setMiddle(RefractionMiddle.WATER);
        mirrorSphere.setShininess(10);
        scene.addObject(mirrorSphere);
    }

    /**
     * Create a chandelier in the scene.
     */
    private static void createChandelier() {
        // Front
        Vec3d top = new Vec3d(0, 2.3, -1.5);
        Vec3d bottom1 = new Vec3d(-0.5, 2, -1.7);
        Vec3d bottom2 = new Vec3d(0, 2, -1.2);
        Vec3d bottom3 = new Vec3d(0.5, 2, -1.7);

        Triangle triangle1 = new Triangle(bottom1, top, bottom2);
        triangle1.setMaterialColor(ColorRGBA.RED);
        Triangle triangle2 = new Triangle(bottom2, top, bottom3);
        triangle2.setMaterialColor(ColorRGBA.GREEN);
        Triangle triangle3 = new Triangle(bottom3, top, bottom1);
        triangle3.setMaterialColor(ColorRGBA.BLUE);

        scene.addObject(triangle1);
        scene.addObject(triangle2);
        scene.addObject(triangle3);

        // Back
        top = new Vec3d(-0.5, 2.3, 1.7);
        bottom1 = new Vec3d(-1, 2, 1.9);
        bottom2 = new Vec3d(-0.5, 2, 1.4);
        bottom3 = new Vec3d(0, 2, 1.9);

        triangle1 = new Triangle(bottom1, top, bottom2);
        triangle1.setMaterialColor(ColorRGBA.RED);
        triangle2 = new Triangle(bottom2, top, bottom3);
        triangle2.setMaterialColor(ColorRGBA.GREEN);
        triangle3 = new Triangle(bottom3, top, bottom1);
        triangle3.setMaterialColor(ColorRGBA.BLUE);

        scene.addObject(triangle1);
        scene.addObject(triangle2);
        scene.addObject(triangle3);
    }

    /**
     * Create two mirror triangles in the scene.
     */
    private static void createMirrorTriangles() {
        // Front
        Triangle triangleLeftMirror = new Triangle(
                new Vec3d(-2, 2.3, -2),
                new Vec3d(-2, 0, -1.5),
                new Vec3d(-1.5, 0, -2.2)
        );
        triangleLeftMirror.setReflectiveCoef(0.5f);
        triangleLeftMirror.setMiddle(RefractionMiddle.GLASS);
        triangleLeftMirror.setTransparencyCoef(0.5f);

        Triangle triangleRightMirror = new Triangle(
                new Vec3d(2, 1.5, -1.5),
                new Vec3d(1.7, 0, -2),
                new Vec3d(2, 0, -1.5)
        );
        triangleRightMirror.setMaterialColor(ColorRGBA.WHITE);
        triangleRightMirror.setReflectiveCoef(0.7f);

        scene.addObject(triangleLeftMirror);
        scene.addObject(triangleRightMirror);


        // Back
        triangleLeftMirror = new Triangle(
                new Vec3d(-2, 2.3, 2),
                new Vec3d(-2, 0, 1.5),
                new Vec3d(-1.5, 0, 2.2)
        );
        triangleLeftMirror.setReflectiveCoef(0.8f);

        triangleRightMirror = new Triangle(
                new Vec3d(2, 1.5, 1.5),
                new Vec3d(1.7, 0, 2),
                new Vec3d(2, 0, 1.5)
        );
        triangleRightMirror.setMaterialColor(ColorRGBA.BLUE);
        triangleRightMirror.setReflectiveCoef(0.2f);

        scene.addObject(triangleLeftMirror);
        scene.addObject(triangleRightMirror);
    }

    /**
     * Create the lights of the scene.
     */
    private static void createLights() {
        Light light = new Light(new Vec3d(0, 2.20, -1.5), ColorRGBA.WHITE, ColorRGBA.WHITE);
        light.setLightAttenuation(0.5f, 1f, 2f);
        light.setAttenuation(Light.Attenuation.LINEAR);

        Light lightR = new Light(new Vec3d(-0, 1.5, 2.9f), ColorRGBA.RED, ColorRGBA.BLACK);
        lightR.setLightAttenuation(3f, 5f, 2f);
        lightR.setAttenuation(Light.Attenuation.QUADRATIC);
        Light lightG = new Light(new Vec3d(-0.4, 0.8, 2.9f), ColorRGBA.GREEN, ColorRGBA.BLACK);
        lightG.setLightAttenuation(3f, 5f, 2f);
        lightG.setAttenuation(Light.Attenuation.QUADRATIC);
        Light lightB = new Light(new Vec3d(0.4, 0.8, 2.9f), ColorRGBA.BLUE, ColorRGBA.BLACK);
        lightB.setLightAttenuation(3f, 5f, 2f);
        lightB.setAttenuation(Light.Attenuation.QUADRATIC);

        scene.addLight(light);
        scene.addLight(lightR);
        scene.addLight(lightG);
        scene.addLight(lightB);
    }
}

