package objects;

/**
 * Enumeration representing physical middles useful to refraction. Include the index of refraction for each
 * middle.
 */
public enum RefractionMiddle {
    AIR(1f),
    WATER(1.3f),
    GLASS(1.5f),
    DIAMOND(2.4f);

    /// The index of refraction of the middle
    public final float ior;

    /**
     * Construct a middle by giving its index of refraction.
     *
     * @param ior The index of refraction of the middle.
     */
    RefractionMiddle(final float ior) {
        this.ior = ior;
    }
}
