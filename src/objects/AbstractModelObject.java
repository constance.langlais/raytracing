package objects;

import utils.ColorRGBA;
import utils.MathUtils;
import utils.Vec3d;

/**
 * Abstract class representing an object to place in a scene and compute intersections in order to make it
 * visible with ray tracing methods.
 */
public abstract class AbstractModelObject {
    /// The color of the object
    private ColorRGBA materialColor;
    /// The color of the specular shape of the object
    private ColorRGBA specularColor;
    /// The brilliance of the object, useful for specular
    private float shininess;

    /// Determine how reflective is the object (clamped between 0 and 1)
    private float kr;
    /// The physical middle of the object
    private RefractionMiddle middle;
    /// Determine how transparent is the object (clamped between 0 and 1)
    private float kt;

    /**
     * Construct a basic object.
     */
    protected AbstractModelObject() {
        this(ColorRGBA.DARK_GRAY, ColorRGBA.WHITE);

    }

    /**
     * Construct a basic object with the color of its material.
     *
     * @param materialColor The color of the material of the object.
     */
    protected AbstractModelObject(ColorRGBA materialColor) {
        this(materialColor, ColorRGBA.WHITE);
    }

    /**
     * Construct a basic object with the color of its material and the specular color.
     *
     * @param materialColor The color of the material of the object.
     * @param specularColor The specular color of the object.
     */
    protected AbstractModelObject(ColorRGBA materialColor, ColorRGBA specularColor) {
        this.materialColor = materialColor;
        this.specularColor = specularColor;
        this.shininess = 50f;
        this.kr = 0f;
        this.kt = 0f;
        this.middle = RefractionMiddle.AIR;
    }

    /**
     * Compute the intersection between the current object and a half ray cast.
     *
     * @param originPoint    The starting point of the half ray to cast.
     * @param directorVector The direction vector of the ray.
     * @return The first intersection between the ray and the current object or {@code null} if none
     * intersection is found.
     */
    public Double computeIntersection(Vec3d originPoint, Vec3d directorVector) {
        return null;
    }


    /**
     * Calculates or give the normal at a given point to the current object. Consider as if the point belongs
     * to the object.
     *
     * @param point The point to calculate the normal.
     * @return The normal at point {@code point} of the current object.
     */
    public Vec3d getNormalAt(Vec3d point) {
        return null;
    }

    /**
     * @return The material color of the object.
     */
    public ColorRGBA getMaterialColor() {
        return materialColor;
    }

    /**
     * @param materialColor The new material color of the object.
     */
    public void setMaterialColor(final ColorRGBA materialColor) {
        this.materialColor = materialColor;
    }

    /**
     * @return The specular color of the object material.
     */
    public ColorRGBA getSpecularColor() {
        return specularColor;
    }

    /**
     * @param specularColor The new specular color of the object material.
     */
    public void setSpecularColor(final ColorRGBA specularColor) {
        this.specularColor = specularColor;
    }

    /**
     * @return {@code true} if the material of the object is reflective, else {@code false}.
     */
    public boolean isReflective() {
        return kr > MathUtils.ERROR_RANGE;
    }

    /**
     * @param kr The coefficient determining how the material is reflective. 0 = Not reflective. 1 = Fully
     *           reflective.
     */
    public void setReflectiveCoef(final float kr) {
        this.kr = MathUtils.clamp(kr, 0, 1);
    }

    /**
     * @return {@code true} if the material is transparent, else {@code false}.
     */
    public boolean isTransparent() {
        return kt > MathUtils.ERROR_RANGE;
    }

    /**
     * @param kt The coefficient determining how the material is transparent. 0 = Not reflective. 1 = Fully
     *           reflective.
     */
    public void setTransparencyCoef(final float kt) {
        this.kt = MathUtils.clamp(kt, 0, 1);
    }

    /**
     * @return The reflectivity coefficient of the object material.
     */
    public float getKr() {
        return kr;
    }

    /**
     * @return The transparency coefficient of the object material.
     */
    public float getKt() {
        return kt;
    }

    /**
     * @param middle The physical middle of the object.
     */
    public void setMiddle(RefractionMiddle middle) {
        this.middle = middle;
    }

    /**
     * @return The physical middle of the object.
     */
    public RefractionMiddle getMiddle() {
        return middle;
    }

    /**
     * @return The brilliance of the material, useful to describe the specular mark. The higher it is, the
     * more the specular mark is spread out.
     */
    public float getShininess() {
        return shininess;
    }

    /**
     * @param shininess The brilliance of the material, useful to describe the specular mark. The higher it
     *                  is, the more the specular mark is spread out.
     */
    public void setShininess(final float shininess) {
        this.shininess = shininess;
    }
}
