package objects;

import utils.MathUtils;
import utils.Vec3d;

/**
 * Class representing a plane, defined by its normal and the {@code d} parameter of its parametric equation.
 * The multiple ways of creating a plane are available.
 */
public class Plane extends AbstractModelObject {
    /// A normal to the plane
    private final Vec3d normal;
    /// The d parameter of the parametric equation
    private final double d;

    /**
     * Construct a plane with the parameters of its parametric equation.
     *
     * @param a The first parameter.
     * @param b The second parameter.
     * @param c The third parameter.
     * @param d The fourth parameter.
     */
    public Plane(double a, double b, double c, double d) {
        super();
        normal = new Vec3d(a, b, c);
        normal.normalize();
        this.d = d;
    }

    /**
     * Construct a plane with three non-aligned points that belong to the plane.
     *
     * @param pointA The first point.
     * @param pointB The second point.
     * @param pointC The third point.
     */
    public Plane(Vec3d pointA, Vec3d pointB, Vec3d pointC) {
        super();
        normal = new Vec3d(pointA, pointB).crossProduct(new Vec3d(pointA, pointC));
        normal.normalize();
        d = -Vec3d.dotProduct(pointA, normal);

    }

    /**
     * Construct a plane with a point belonging to the plane and a normal to the plane.
     *
     * @param point A point that belong to the plane.
     * @param n     A normal to the plane
     */
    public Plane(Vec3d point, Vec3d n) {
        super();
        normal = n;
        normal.normalize();
        d = -Vec3d.dotProduct(point, n);
    }


    @Override
    public Double computeIntersection(final Vec3d originPoint, final Vec3d directorVector) {
        double dotProduct = Vec3d.dotProduct(normal, directorVector);
        if (dotProduct < MathUtils.ERROR_RANGE && dotProduct > -MathUtils.ERROR_RANGE) {
            // No intersection with the plane
            return null;
        }
        return (-Vec3d.dotProduct(normal, originPoint) - d) / dotProduct;
    }

    @Override
    public Vec3d getNormalAt(final Vec3d point) {
        return normal; // already normalized
    }

    /**
     * @return The normal to the plane.
     */
    public Vec3d getNormal() {
        return normal; // already normalized
    }
}
