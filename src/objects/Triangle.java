package objects;

import utils.MathUtils;
import utils.Vec3d;

/**
 * Class representing a triangle, defined by its three vertices. The order of definition of the vertices is
 * important, it determines the visible and hidden part of the triangle.
 */
public class Triangle extends AbstractModelObject {
    /// The first vertex
    private final Vec3d vertexA;
    /// The second vertex
    private final Vec3d vertexB;
    /// The third vertex
    private final Vec3d vertexC;

    /// The plane containing the triangle
    private final Plane plane;
    /// The normal to the triangle
    private final Vec3d normal;

    /**
     * Construct a triangle by giving its three vertices.
     *
     * @param vertexA The first vertex of the triangle.
     * @param vertexB The second vertex of the triangle.
     * @param vertexC The third vertex of the triangle.
     */
    public Triangle(final Vec3d vertexA, final Vec3d vertexB, final Vec3d vertexC) {
        super();
        this.vertexA = vertexA;
        this.vertexB = vertexB;
        this.vertexC = vertexC;
        this.plane = new Plane(vertexA, vertexB, vertexC);
        this.normal = plane.getNormal(); // already normalized
    }

    @Override
    public Double computeIntersection(final Vec3d originPoint, final Vec3d directorVector) {
        // Compute the intersection to the plane containing the triangle
        Double lambda = plane.computeIntersection(originPoint, directorVector);
        if (lambda == null || lambda <= MathUtils.ERROR_RANGE) {
            return null;
        }

        // Intersection point
        Vec3d intersection = Vec3d.scale(directorVector, lambda).add(originPoint);

        // Compute with barycentric coordinates
        Vec3d interToA = new Vec3d(intersection, vertexA);
        Vec3d interToB = new Vec3d(intersection, vertexB);
        Vec3d interToC = new Vec3d(intersection, vertexC);
        double alpha = Vec3d.crossProduct(interToB, interToC).dotProduct(normal);
        double beta = Vec3d.crossProduct(interToC, interToA).dotProduct(normal);
        double gamma = Vec3d.crossProduct(interToA, interToB).dotProduct(normal);

        if ((alpha > 0) && (beta > 0) && (gamma > 0)) {
            // The point is inside the triangle
            return lambda;
        }
        return null;
    }

    @Override
    public Vec3d getNormalAt(final Vec3d point) {
        return normal; // already normalized
    }
}
