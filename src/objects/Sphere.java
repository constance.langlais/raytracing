package objects;

import utils.MathUtils;
import utils.Vec3d;

/**
 * Class representing a sphere, defined by its center and its radius.
 */
public class Sphere extends AbstractModelObject {
    /// The center of the sphere
    private final Vec3d center;
    /// The radius of the sphere
    private final double radius;

    /**
     * Construct a sphere from its center point and its radius.
     *
     * @param center The center point of the sphere.
     * @param radius The radius of the sphere.
     */
    public Sphere(Vec3d center, double radius) {
        super();
        this.center = center;
        this.radius = radius;
    }

    @Override
    public Double computeIntersection(final Vec3d originPoint, final Vec3d directorVector) {
        Vec3d centerToOrigin = new Vec3d(center, originPoint);
        double a = directorVector.lengthSquare();
        double b = 2 * (Vec3d.dotProduct(directorVector, centerToOrigin));
        double c = centerToOrigin.lengthSquare() - radius * radius;
        double delta = b * b - 4 * a * c;

        if (delta < 0) {
            // No solutions/roots
            return null;
        }

        if (delta < MathUtils.ERROR_RANGE) {
            // Half-ray tangent to the sphere
            return -b / (2 * a);
        }

        double lambda1 = (-b - Math.sqrt(delta)) / (2 * a);
        double lambda2 = (-b + Math.sqrt(delta)) / (2 * a);
        if (lambda2 <= 0) {
            // No intersection with the sphere
            return null;
        } else if (lambda1 < MathUtils.ERROR_RANGE) {
            // The origin point is in the sphere and the ray comes out of the sphere
            return lambda2;
        } else {
            // The origin point is out of the sphere
            return lambda1;
        }
    }

    @Override
    public Vec3d getNormalAt(final Vec3d point) {
        return Vec3d.sub(point, center).normalize();
    }
}
