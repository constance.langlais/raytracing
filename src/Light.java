import utils.ColorRGBA;
import utils.Vec3d;

/**
 * Class representing a light source. It is defined by its position in a scene and the color/contribution of
 * its components : ambient, diffuse and specular.
 */
public class Light {
    /**
     * Enumeration representing the type of attenuation used.
     */
    public enum Attenuation {
        CONSTANT, LINEAR, QUADRATIC, NONE
    }

    /// The position of the light in the scene
    private Vec3d position;
    /// The color of the ambient light
    private static ColorRGBA ambient = ColorRGBA.DARK_GRAY;
    /// The color of the diffuse light
    private ColorRGBA diffuse;
    /// The color of the specular shape
    private ColorRGBA specular;

    /// Attenuations of light with respect to distance between lighted point and light source
    /// The constant attenuation
    private float constantAttenuation;
    /// The linear attenuation
    private float linearAttenuation;
    /// The quadratic attenuation
    private float quadraticAttenuation;
    /// The attenuation to compute the lights with
    private Attenuation attenuation;

    /**
     * Construct a basic light with default component colors.
     */
    public Light() {
        this(new Vec3d(0, 0, 0), ColorRGBA.LIGHT_GRAY, ColorRGBA.WHITE);
    }

    /**
     * Construct a light by giving its position and default colors.
     *
     * @param position The position of the light.
     */
    public Light(final Vec3d position) {
        this(position, ColorRGBA.LIGHT_GRAY, ColorRGBA.WHITE);
    }

    /**
     * Construct a light by giving its position, and intensities of its components.
     *
     * @param position The position of the light in the scene.
     * @param diffuse  The diffuse color of the light.
     * @param specular The specular color of the light.
     */
    public Light(final Vec3d position, final ColorRGBA diffuse, final ColorRGBA specular) {
        this.position = position;
        this.diffuse = diffuse;
        this.specular = specular;
        this.constantAttenuation = 1f;
        this.linearAttenuation = 1f;
        this.quadraticAttenuation = 1f;
        this.attenuation = Attenuation.NONE;
    }

    /**
     * Compute the ambient color of an object of the given color.
     *
     * @param materialColor The material color of the object to compute the ambient color.
     * @return The ambient color component of the object.
     */
    public static ColorRGBA computeAmbientColor(final ColorRGBA materialColor) {
        return ColorRGBA.mult(ambient, materialColor);
    }

    /**
     * Compute the diffuse color at a point {@code intersection} with the normal {@code normal} of a given
     * object color.
     *
     * @param intersection  The intersection point where to compute the diffuse color.
     * @param normal        The normal at the point.
     * @param materialColor The material color of the object at this point.
     * @return The diffuse color component of the object.
     */
    public ColorRGBA computeDiffuseColor(final Vec3d intersection, final Vec3d normal,
                                         final ColorRGBA materialColor) {
        Vec3d interToSource = new Vec3d(intersection, position).normalize();
        float lightweight = (float) Math.max(Vec3d.dotProduct(normal, interToSource), 0.0);
        return ColorRGBA.mult(diffuse, lightweight).mult(materialColor);
    }

    /**
     * Compute the specular color at point {@code intersection} with the normal {@code normal} of a given
     * object specular color.
     *
     * @param observerPos   The position of the observer.
     * @param intersection  The intersection point where to compute the specular color.
     * @param normal        The normal at the point.
     * @param specularColor The specular color of the object at this point.
     * @param shininess     The shininess of the object.
     * @return The specular color component of the object.
     */
    public ColorRGBA computeSpecularColor(final Vec3d observerPos, final Vec3d intersection,
                                          final Vec3d normal,
                                          final ColorRGBA specularColor, final float shininess) {
        Vec3d specularDir = new Vec3d(intersection, position).add(observerPos).normalize();
        float specularWeight = (float) Math.max(Vec3d.dotProduct(specularDir, normal.normalize()), 0.0);// n.h
        return ColorRGBA.mult(specular, specularColor)
                .mult((float) Math.pow(specularWeight, shininess));
    }

    /**
     * Compute the attenuation due to distance from the object.
     *
     * @param intersection The intersection point to compute the light attenuation.
     * @return The light attenuation factor.
     */
    public float computeAttenuation(final Vec3d intersection) {
        float d = (float) new Vec3d(intersection, position).length();
        switch (attenuation) {
            case CONSTANT:
                return constantAttenuation;
            case LINEAR:
                return linearAttenuation / d;
            case QUADRATIC:
                return quadraticAttenuation / (d * d);
            case NONE:
            default:
                return 1f;
        }
    }

    /**
     * Set the light attenuation coefficients.
     *
     * @param constant  The constant attenuation.
     * @param linear    The linear attenuation.
     * @param quadratic The quadratic attenuation.
     */
    public void setLightAttenuation(final float constant, final float linear, final float quadratic) {
        this.constantAttenuation = constant;
        this.linearAttenuation = linear;
        this.quadraticAttenuation = quadratic;
    }

    /**
     * @return The position of the light.
     */
    public Vec3d getPosition() {
        return position;
    }

    /**
     * @param position The new position of the light.
     */
    public void setPosition(final Vec3d position) {
        this.position = position;
    }

    /**
     * @param ambient The ambient light color.
     */
    public static void setAmbient(final ColorRGBA ambient) {
        Light.ambient = ambient;
    }

    /**
     * @param diffuse The diffuse light color.
     */
    public void setDiffuse(final ColorRGBA diffuse) {
        this.diffuse = diffuse;
    }

    /**
     * @param specular The specular light color.
     */
    public void setSpecular(final ColorRGBA specular) {
        this.specular = specular;
    }

    /**
     * @param attenuation The light attenuation to use.
     */
    public void setAttenuation(final Attenuation attenuation) {
        this.attenuation = attenuation;
    }
}
